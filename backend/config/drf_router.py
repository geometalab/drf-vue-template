from rest_framework_extensions.routers import ExtendedDefaultRouter
from rest_framework.routers import Route, DynamicListRoute, DynamicDetailRoute


class RESTfulRouter(ExtendedDefaultRouter):
    """
    A router for RESTful APIs, which uses trailing slashes on resources lists, and
    no trailing slashes on resources endpoints.
    See https://en.wikipedia.org/wiki/Representational_state_transfer#Example for more
    explanations.
    """
    routes = [
        # List route.
        Route(
            url=r'^{prefix}{trailing_slash}$',
            mapping={
                'get': 'list',
                'post': 'create',
            },
            name='{basename}-list',
            initkwargs={'suffix': 'List'}
        ),
        # Dynamically generated list routes.
        # Generated using @list_route decorator
        # on methods of the viewset.
        DynamicListRoute(
            url=r'^{prefix}/{methodname}$',
            name='{basename}-{methodnamehyphen}',
            initkwargs={}
        ),
        # Detail route.
        Route(
            url=r'^{prefix}/{lookup}$',
            mapping={
                'get': 'retrieve',
                'put': 'update',
                'patch': 'partial_update',
                'delete': 'destroy',
            },
            name='{basename}-detail',
            initkwargs={'suffix': 'Instance'}
        ),
        # Dynamically generated detail routes.
        # Generated using @detail_route decorator on methods of the viewset.
        DynamicDetailRoute(
            url=r'^{prefix}/{lookup}/{methodname}$',
            name='{basename}-{methodnamehyphen}',
            initkwargs={}
        ),
    ]
