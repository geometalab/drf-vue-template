# Django DRF and Vue Application Template

The Vue frontend uses the default `webpack` starter template using
the `vue-cli` with some additions:

* lodash - JavaScript Library for very common used cases
* vue-material - Material Design components for vuejs
* vue-resource - Simple abstraction for Ajax-Calls
* vue-spinner - a collection of spinners to animate waiting time

## Structure

Javascript Frontend Code can be found in the `frontend` folder.

Django/Python Backend Code can be found in the `backend` folder.

## Prerequisites

The entire project runs inside docker, development and production.

You need to have installed 
[Docker](https://www.docker.com/) [1.13.1 or higher] 
and 
[docker-compose](https://docs.docker.com/compose/) [1.11.2 or higher].

## Usage

Clone this project, delete the `.git/` folder and add this repo.

```bash
git clone git@gitlab.com:geometalab/drf-vue-template.git
rm -rf .git/
git init .
git remote add origin <url-of-your-new-remote>
git add .
git commit -m 'project setup'
git push --all
```

### Hints

* Renaming the package and it's version for the frontend:
    ```json
    {
      "name": "your-project",
      "version": "0.0.1",
      "description": "my project....",
      "author": "some author <his@email.com>",
      ...
    }
    ```
* using `bootstrap-vue` instead of `vue-material`
  * change CSP Header in nginx-templates (production and development)
    ```
        style-src 'self' 'unsafe-inline' https://maxcdn.bootstrapcdn.com http://maxcdn.bootstrapcdn.com;
        font-src 'self' https://maxcdn.bootstrapcdn.com http://maxcdn.bootstrapcdn.com data:;
    ```
  * replace the font and css-links with the `maxcdn.bootstrapcdn.com` css-link

### Using the `Makefile` 

* build images: `make build`
* upgrade requirements: `make requirements-upgrade`

### Running the project

#### In Development

```shell
make build
docker-compose build
docker-compose up
```

#### In Production

This section is in progress, the `docker-compose-prod.yml` is unsuitable
for production currently.

Generally, when the `yml` file has been set up correctly, one should only need
to call:

```shell
make build
docker-compose -f docker-compose-prod.yml up -d
```
