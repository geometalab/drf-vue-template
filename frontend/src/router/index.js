import Vue from 'vue';
import VueMaterial from 'vue-material';
import Router from 'vue-router';
import Hello from '@/components/Hello';

Vue.use(Router);
Vue.use(VueMaterial);

Vue.material.registerTheme('default', {
  primary: 'blue',
  accent: 'red',
  warn: 'red',
  background: 'white',
});

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello,
    },
  ],
});
